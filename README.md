# Remicle

Distributed sensing in aeroelastic structures. 

## Project Description

We think the feathers on birds' wings exhibit local adaptation to the flow regime they are experiencing in order to prevent flow separation [Carruthers et. al. 2007](http://jeb.biologists.org/content/jexbio/210/23/4136.full.pdf). An important component of this behavior is the ability to rapidly sense local information about the flow, fuse that information with neighbors' data, and make decisions on that total information without centralized control.  

This work is therefore an exploration of distributed sensing architectures for aeroelastic control. It combines low-cost MEMS sensors with a rapid prototyping approach that allows the testing the network architectures for flow estimation in steady and unsteady regimes. 

## Previous Work

[Previous work](https://www.overleaf.com/read/bjtmdwhxwrjm) has focused on the feasibility of distributed sensing, and applied 80 [commerical MEMS pressure sensors](https://www.digikey.com/product-detail/en/infineon-technologies/DPS310XTSA1/DPS310XTSA1CT-ND/6565756) to a 14ft span wing. Results showed accurate estimation of lift using a distributed algorithm that required only local information regarding the flow. 

## Workflow

  1. Manufacture of backing + copper laminate
  2. Kirigami cutting of laminate
  3. Integration of electronics components including sensor nodes and modules
  4. (Manual for now) assembly of integrated components into airfoil
  5. Testing of airfoil

## Potential Parts

  1. **Sensor Node**: The [Fanstel BC832](https://www.arrow.com/en/products/bc832/fanstel-corporation) is a 8mm x 9mm module running a NRF52832. Tiny!
  2. **Pressure Sensor + Gyro + Accelerometer**: [ICM-20789](https://www.digikey.com/product-detail/en/tdk-invensense/ICM-20789/1428-1133-1-ND/7794714)
