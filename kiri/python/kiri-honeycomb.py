#!/usr/bin/python

import matplotlib.pyplot as plt
from matplotlib import collections as mc
import numpy as np

def naca(t,x):
	return 5*t*(0.2969*np.sqrt(x)\
		       -0.1260*x\
		       -0.3516*np.power(x,2)\
		       +0.2843*np.power(x,3)\
		       -0.1015*np.power(x,4))

## USAGE
#naca(0.1,0.5) 								<< Single Val
#naca(0.1,np.array([0.1,0.2,0.3,0.4,0.5]))  << Array of Vals

def test_t(x):
	return 0.002*np.power(x,2)-0.4*x+40

def test_u(x):
	return 10*np.sin(2*np.pi*x/200)

def is_cross_section_foldable(uyvals,tyvals):
	uvalspos = (np.pad(uyvals,(0,1),mode='constant')[1:]+\
		        np.pad(uyvals,(1,0),mode='constant')[:-1]-\
	            2*uyvals)[range(0,int(len(uyvals)/2)+1,2)][1:]
	tvalspos = (2*np.pad(tyvals,(0,1),mode='constant')[1:]-\
		          np.pad(tyvals,(0,2),mode='constant')[2:]-\
	              tyvals)[range(0,int(len(tyvals)/2)+1,2)]
	#tvalspos = -1*tvalspos

	return all(tvalspos >= 0) and all(uvalspos >= 0)

def get_foldable_honeycomb(uyvals,tyvals):
	tyvals_odd  = 0.5*(tyvals[range(0,len(tyvals),2)]\
		              +np.pad(tyvals[range(0,len(tyvals),2)],(0,1),mode='constant')[1:]) 
	tyvals = np.ravel(np.vstack((tyvals[range(0,len(tyvals),2)],tyvals_odd)).T)[0:-1]
	
	uyvals_even = 0.5*((np.pad(uyvals,(0,1),mode='constant')[1:])[range(0,len(uyvals),2)]\
		              +(np.pad(uyvals,(1,0),mode='constant')[:-1])[range(0,len(uyvals),2)])[1:-1] 
	uyvals = np.ravel(np.vstack((np.pad(uyvals_even,(1,1),mode='constant'),np.pad(uyvals[range(1,len(uyvals),2)],(0,1),mode='constant'))).T)[0:-1]
	
	return uyvals,tyvals

def calculate_ab_values(uyvals, tyvals):
	lyvals = (tyvals-uyvals)
	tcumsum = ((2*np.pad(tyvals,(0,1),mode='constant')[1:]\
		     -np.pad(uyvals,(0,2),mode='constant')[2:]\
		     -uyvals)[range(0,len(uyvals),2)])[:-1]

	avals_3m = np.cumsum((2*np.pad(tyvals,(0,1),mode='constant')[1:]\
		                   -np.pad(uyvals,(0,2),mode='constant')[2:]\
		                   -uyvals)[range(0,len(uyvals),2)])[:-1]
	avals_3m = np.pad(avals_3m,(1,0),mode='constant')
	avals_3m_p1 = avals_3m+lyvals[range(0,len(uyvals),2)]
	avals_3m_m1 = avals_3m-lyvals[range(0,len(uyvals),2)]
	
	avals = np.ravel(np.vstack((avals_3m_m1,avals_3m,avals_3m_p1)).T)
	bvals_3m = (np.pad(2*np.cumsum((np.pad(tyvals,(0,1),mode='constant')[1:]\
						           -np.pad(uyvals,(0,2),mode='constant')[2:])[range(0,len(uyvals),2)])\
	                  ,(1,0),mode='constant')[0:-1]\
	            - uyvals[0]\
	            + (np.pad(uyvals,(0,1),mode='constant')[1:])[range(0,len(uyvals),2)])
	

	bvals_3m_p1 = bvals_3m +   (np.pad(lyvals,(0,1),mode='constant')[1:])[range(0,len(lyvals),2)]
	bvals_3m_p2 = bvals_3m + 2*(np.pad(lyvals,(0,1),mode='constant')[1:])[range(0,len(lyvals),2)]

	bvals = np.ravel(np.vstack((bvals_3m,bvals_3m_p1,bvals_3m_p2)).T)[0:-1]
	
	return avals[1:],bvals[:-1]

def honeycomb(t,width,numcells):
	cellsize = width/numcells
	naca_xvals = np.linspace(0.1,1,2*numcells+1)
	naca_yvals = naca(t,naca_xvals)
	naca_uyvals_scaled = -naca_yvals*width
	naca_tyvals_scaled =  naca_yvals*width

	#naca_tyvals_scaled = test_t(width*naca_xvals)
	#naca_uyvals_scaled = test_u(width*naca_xvals)
	
	if(not is_cross_section_foldable(naca_uyvals_scaled,naca_tyvals_scaled)):
		naca_uyvals_scaled,naca_tyvals_scaled = get_foldable_honeycomb(naca_uyvals_scaled,naca_tyvals_scaled)
	
	naca_avals, naca_bvals = calculate_ab_values(naca_uyvals_scaled,naca_tyvals_scaled)

	print(naca_avals)
	print(naca_bvals)
	a_base_pts = np.vstack((naca_avals,np.zeros(np.shape(naca_avals)))).T
	a_top_pts = np.vstack((naca_avals,np.ones(np.shape(naca_avals)))).T
	a_horizontal_pts = np.vstack((a_base_pts,np.array([[0,0]])))[1:]
	
	a_segments = np.stack((a_base_pts,a_top_pts),axis=1)
	a_horizontal_segments = np.stack((a_base_pts,a_horizontal_pts),axis=1)
	a_horizontal_segments = np.delete(a_horizontal_segments,range(1,len(a_horizontal_segments),3),axis=0)
	
	valleys = np.copy(a_segments[range(3,len(a_segments),3)])

	b_base_pts = np.vstack((naca_bvals,np.zeros(np.shape(naca_bvals))+2)).T
	b_top_pts = np.vstack((naca_bvals,np.ones(np.shape(naca_bvals))+2)).T
	b_horizontal_pts = np.vstack((b_base_pts,np.array([[0,0]])))[1:]

	b_segments = np.stack((b_base_pts,b_top_pts),axis=1)
	b_horizontal_segments = np.stack((b_base_pts,b_horizontal_pts),axis=1)
	b_horizontal_segments = np.delete(b_horizontal_segments,range(2,len(b_horizontal_segments),3),axis=0)

	mountains = np.copy(b_segments[range(1,len(b_segments),3)])

	b_horizontal_segments = b_horizontal_segments[:-1]
	valleys = np.vstack((valleys,b_horizontal_segments))
	b_horizontal_segments.T[1] = b_horizontal_segments.T[1]+1
	valleys = np.vstack((valleys,b_horizontal_segments))

	cuts = np.delete(a_segments,range(3,len(a_segments),3),axis=0)
	cuts = np.vstack((cuts,a_horizontal_segments))
	cuts = np.vstack((cuts,np.delete(b_segments,range(1,len(b_segments),3),axis=0)))
	
	a_horizontal_segments.T[1] = a_horizontal_segments.T[1]+1
	mountains = np.vstack((mountains,a_horizontal_segments))

	a_merge_indices = np.array(range(len(a_top_pts)))
	a_merge_indices = np.insert(a_merge_indices,range(3,len(a_merge_indices),3),range(3,len(a_merge_indices),3))
	a_merge_indices = a_merge_indices[:-1]

	b_merge_indices = np.array(range(len(b_base_pts)))
	b_merge_indices = np.insert(b_merge_indices,range(1,len(b_merge_indices),3),range(1,len(b_merge_indices),3))

	diag_segments = np.stack((a_top_pts[a_merge_indices],b_base_pts[b_merge_indices]),axis=1)
	
	cuts = np.vstack((cuts,diag_segments))

	lcc = mc.LineCollection(cuts,color='grey')
	lcv = mc.LineCollection(valleys,color='b')
	lcm = mc.LineCollection(mountains,color='r')

	fig, ax = plt.subplots()
	ax.add_collection(lcc)
	ax.add_collection(lcv)
	ax.add_collection(lcm)
	ax.autoscale()
	ax.set_ylim((0,4))
	plt.title("Kirigami NACA 0010 Airfoil")
	plt.show()
	#a_valley = naca_avals[range(3,len(naca_avals),3)]
	#a_valley_base = np.vstack((avalley,np.zeros(np.shape(naca_avals)))).T
	#for i,abasept in enumerate(abasepts):
	#	if i%3 == 1:
	'''
	abasepts = np.vstack((naca_avals,np.zeros(np.shape(naca_avals)))).T
	atoppts = np.vstack((naca_avals,np.ones(np.shape(naca_avals)))).T
	asegs = np.stack((abasepts,atoppts),axis=1)
	
	bbasepts = np.vstack((naca_bvals,np.zeros(np.shape(naca_bvals))+2)).T
	btoppts = np.vstack((naca_bvals,np.ones(np.shape(naca_bvals))+2)).T
	csegs = np.stack((bbasepts,btoppts),axis=1)



	print(np.shape(atoppts),np.shape(bbasepts))
	bsegs = np.stack((atoppts,bbasepts),axis=1)

	lcb = mc.LineCollection(bsegs)
	ax.add_collection(lcb)
	ax.add_collection(lcc)
	#plt.plot(width*naca_xvals,naca_tyvals_scaled)
	#plt.plot(width*naca_xvals,naca_uyvals_scaled)
	#for i in range(len(naca_tyvals_scaled)):
	#	print(i,naca_tyvals_scaled[i],naca_uyvals_scaled[i])
	'''




honeycomb(0.2,200,10)
